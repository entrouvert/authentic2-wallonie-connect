# -*- coding: utf-8 -*-
#
# authentic2-wallonie-connect - Authentic2 plugin for the Wallonie Connect usecase
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from authentic2.a2_rbac.models import OrganizationalUnit as OU
from authentic2.custom_user.models import User
from authentic2_idp_oidc.models import OIDCClient

from django.core.management import call_command
from django.contrib.auth import authenticate


def test_wc_base_import_command(db, tmpdir):
    password = "admin"
    username = "user-1"
    content = {
        "data": [
            {
                "locality": {"name": "Liège", "slug": "liege"},
                "services": [
                    {
                        "name": "Service",
                        "slug": "service",
                        "frontchannel_logout_uri": "",
                        "post_logout_redirect_uris": ["https://service.example.com/"],
                        "redirect_uris": ["https://service.example.com/"],
                        "has_api_access": True,
                    }
                ],
                "users": [
                    {
                        "username": "user-1",
                        "email": "user-1@example.com",
                        "first_name": "Jean",
                        "last_name": "Darmette",
                        "password": "{SSHA}m8eqVfCP2tk1w/g2LT9IvPcOsoBzYWx0",
                    }
                ],
            }
        ],
        "version": "1",
    }
    temp_json = tmpdir / "users.json"
    with temp_json.open("w") as fd:
        json.dump(content, fd)

    assert OU.objects.count() == 1
    assert User.objects.count() == 0
    call_command("wc-base-import", str(temp_json), no_dry_run=True)

    assert OU.objects.count() == 2
    ou = OU.objects.get(slug="liege")
    assert User.objects.count() == 1
    user = User.objects.get()
    assert user.username == username
    assert user.ou.name == "Liège"
    assert user.ou.slug == "liege"
    # assert user.check_password(password)
    # assert authenticate(username=username, password=password, ou=ou) == user

    client = OIDCClient.objects.get()
    assert client.client_id
    assert client.client_secret
    assert client.name == "Service"
    assert client.slug == "service"
    assert client.frontchannel_logout_uri == ""
    assert client.redirect_uris == "https://service.example.com/"
    assert client.post_logout_redirect_uris == "https://service.example.com/"
    assert set(client.oidcclaim_set.values_list("name", "value", "scopes")) == set(
        [
            ("given_name", "first_name", "profile"),
            ("family_name", "last_name", "profile"),
            ("email", "email", "email"),
        ]
    )

    with temp_json.open() as fd:
        updated_content = json.load(fd)
    assert "client_id" in updated_content["data"][0]["services"][0]
    assert "client_secret" in updated_content["data"][0]["services"][0]
    assert "uuid" in updated_content["data"][0]["users"][0]
