# authentic2-wallonie-connect - Authentic2 plugin for the Wallonie Connect usecase
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import django_webtest


@pytest.fixture
def app(request, db, settings, tmpdir):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    settings.MEDIA_DIR = str(tmpdir.mkdir('media'))
    return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost', 'wsgi.url_scheme': 'https'})


@pytest.fixture(autouse=True)
def clear_event_type_cache(db):
    from authentic2.apps.journal.models import event_type_cache
    yield
    event_type_cache.cache.clear()
