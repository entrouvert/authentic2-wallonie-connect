import os

ALLOWED_HOSTS = ['localhost']
LANGUAGE_CODE = 'en'

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': 'authentic2',
    }
}

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]

A2_RBAC_MANAGED_CONTENT_TYPES = ()
A2_RBAC_ROLE_ADMIN_RESTRICT_TO_OU_USERS = True

if 'authentic2_wallonie_connect' not in INSTALLED_APPS:
    INSTALLED_APPS += ('authentic2_wallonie_connect',)
