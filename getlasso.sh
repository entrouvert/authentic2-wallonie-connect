#!/bin/sh

# Get venv site-packages path
DSTDIR=`python -c 'import sysconfig; print(sysconfig.get_path("purelib"))'`

# Get not venv site-packages path
# Remove first path (assuming that is the venv path)
NONPATH=`echo $PATH | sed 's/^[^:]*://'`
SRCDIR=`PATH=$NONPATH python -c 'import sysconfig; print(sysconfig.get_path("purelib"))'`

# Clean up
rm -f $DSTDIR/lasso.*
rm -f $DSTDIR/_lasso.*

# Link
ln -sv $SRCDIR/lasso.py $DSTDIR
ln -sv $SRCDIR/_lasso.* $DSTDIR

exit 0

