# coding: utf-8
#
# authentic2-wallonie-connect - Authentic2 plugin for the Wallonie Connect usecase
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import apps
from django.db import router
from django.db.models.signals import post_save, post_migrate
from django.contrib.contenttypes.models import ContentType

from authentic2.custom_user.models import User
from authentic2.a2_rbac.models import (
    OrganizationalUnit as OU,
    Role,
    Permission,
    RESET_PASSWORD_OP,
    ACTIVATE_OP,
    CHANGE_EMAIL_OP,
    ADMIN_OP,
    CHANGE_OP,
    ADD_OP
)
from authentic2.a2_rbac.utils import get_operation
from authentic2.models import Service


def init(app_config):
    post_migrate.connect(post_migrate_handler, sender=apps.get_app_config('a2_rbac'))
    post_save.connect(ou_post_save_handler, sender=OU)


def post_migrate_handler(using, **kwargs):
    if not router.allow_migrate(using, Role):
        return
    create_base_roles()
    update_ous_roles()


def create_base_roles():
    admin, created = Role.objects.get_or_create(
        slug='_imio-wc-admin',
        ou=None,
        defaults={
            'name': 'Administrateur iMio',
        })
    set_permissions(admin, [
        (User, None, [ADMIN_OP]),
        (Role, None, [ADMIN_OP]),
        (Service, None, [ADMIN_OP]),
        (OU, None, [ADMIN_OP]),
    ])


def ou_post_save_handler(instance, **kwargs):
    update_ou_roles(instance)


def update_ous_roles():
    for ou in OU.objects.all():
        update_ou_roles(ou)


def update_ou_roles(ou):
    if not ou.username_is_unique:
        ou.username_is_unique = True
        ou.save()
    # Create admin of users
    user_admin, created = Role.objects.get_or_create(
        slug='_imio-wc-user-admin',
        ou=ou,
        defaults={
            'name': 'Administrateur des utilisateurs',
        })
    set_permissions(user_admin, [(User, ou, [
        ADD_OP, CHANGE_OP, RESET_PASSWORD_OP,
        ACTIVATE_OP, CHANGE_EMAIL_OP])])

    # Create admin of roles
    role_admin, created = Role.objects.get_or_create(
        slug='_imio-wc-role-admin',
        ou=ou,
        defaults={
            'name': 'Administrateur des rôles',
        })
    set_permissions(role_admin, [(Role, ou, [CHANGE_OP])])


def set_permissions(role, model_scope_ops):
    permissions = set()
    for model, scope, operations in model_scope_ops:
        for operation in operations:
            permission, created = Permission.objects.get_or_create(
                operation=get_operation(operation),
                ou=scope,
                target_ct=ContentType.objects.get_for_model(ContentType),
                target_id=ContentType.objects.get_for_model(model).pk)
            permissions.add(permission)
    if set(role.permissions.all()) != permissions:
        role.permissions.set(permissions)

